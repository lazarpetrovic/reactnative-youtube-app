import React, { Component } from 'react';
import { Platform, StyleSheet, View } from 'react-native';
import YouTube from 'react-native-youtube';
import { Container, Header, Left, Body, Right, Button, Item, Input, Text, Title, Content, List, ListItem, Thumbnail } from 'native-base';
import { google } from 'react-native-simple-auth';
import LogIn from '../components/LogIn';
import SearchList from './SearchList';
import { Icon } from 'react-native-elements';



const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

const APIkey = 'AIzaSyDHc1X5YRhCqdeEofH2KeXkj22VOpfjPHM';


export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      videos: [],
      searchText: '',
      currentVideo: '',
      changeColor: false
    } 
    this.logInFunc = this.logInFunc.bind(this);
    this.renderVideo = this.renderVideo.bind(this);
    this.onSearchChange = this.onSearchChange.bind(this);
    this.onItemPress = this.onItemPress.bind(this);
    this.videoSearch = this.videoSearch.bind(this);
    this.renderSearchList = this.renderSearchList.bind(this);
  }

  componentWillMount() {
    fetch(`https://www.googleapis.com/youtube/v3/search?part=snippet&type=youtube%23video&key=${APIkey}&q=${ 'Tech house' }&maxResults=50`)
      .then(response => response.json())
      .then(data => {
        console.log('ovo je data', data);
        this.setState({ videos: data.items });
      });
       // this.videoSearch();
      setTimeout(() => {
        console.log('this is state changeColor', this.state.changeColor);
        this.setState({ changeColor: true})}, 5000);
        console.log('this is state changeColor', this.state.changeColor);
  }

  logInFunc() {
    console.log('passed login func');
    google({
      appId: '786858013402-kj2nhcc7shaiah5s800uvjs5tma83jjh.apps.googleusercontent.com',
      callback: 'org.YtApp:/oauth2redirect' ,
    }).then((info) => {
      console.log('ovo je info os useru');
      // info.user - user details from the provider
      // info.credentials - tokens from the provider
    }).catch((error) => {
      // error.code
      // error.description
    });
  }

  renderVideo() {
    if (this.state.currentVideo) {
      return (
        <YouTube
          videoId={this.state.currentVideo.id.videoId}
          play={true}
          fullscreen={true}
          loop={true}
          onReady={e => this.setState({ isReady: true })}
          onChangeState={e => this.setState({ status: e.state })}
          onChangeQuality={e => this.setState({ quality: e.quality })}
          onError={e => this.setState({ error: e.error })}
          style={{ 
            alignSelf: "stretch", 
            height: 300, 
            shadowRadius: 10, 
            shadowColor: "pink", 
            shadowOpacity: 1 
          }}
        />
      );
    }
    return <Text
      style={{ 
        paddingTop: this.state.changeColor? 120:10, 
        alignSelf: "stretch", 
        height: this.state.changeColor? 300: 50,
        color: this.state.changeColor?  "black":"#bec647", 
        backgroundColor: this.state.changeColor?  "#bec647":"black", 
        shadowRadius: 10, 
        shadowColor: "#ccc",
        }}>
      Select Any video from list or search for new video...
          </Text>
  }

  renderSearchList() {
    if (this.state.videos.length) {
      return (
        this.state.videos.map((video, index) => {
          return (
              <ListItem onPress={() => { this.onItemPress(video) }} button key={video.id.videoId} >
                <Thumbnail square size={80} source={{ uri: video.snippet.thumbnails.default.url }} />
                <Body>
                  <Text>{video.snippet.title}</Text>
                  {/* <Text note>{video.snippet.description}</Text> */}
                </Body>
              </ListItem>
          )
        }
        )
      );
    }
    return (
      <Text>Search results will appear here.</Text>
    )
  }

  videoSearch() {
    // console.log('ovo je search text', this.state.searchText);
    fetch(`https://www.googleapis.com/youtube/v3/search?part=snippet&type=youtube%23video&key=${APIkey}&q=${this.state.searchText}&maxResults=50`)
      .then(response => response.json())
      .then(data => {
        console.log('ovo je dataa', data);
        this.setState({ videos: data.items });
      })
  }
  onSearchChange(value) {
    this.setState({ searchText: value })
  }

  onItemPress(video) {
    // console.log('oovoo je video id', video);
    // console.log('ovo je trenutni video', this.state.currentVideo);
    this.setState({
      currentVideo: video
    });
  }
  render() {
    // console.log('ovo je state videos', this.state.videos);
    return (
      <Container>
        <Header searchBar rounded style={styles.searchBarHeader}>
            <Item >
              <Icon name="search" />
              <Input placeholder="Search" value={this.state.searchText} onChangeText={this.onSearchChange} />
            </Item>
            <Button name="user" onPress={this.videoSearch} style={styles.buttonStyle}>
              <Text>Search</Text>
            </Button>
          </Header>
        <Container>
          <View style={styles.headLoginView}>
            <Text>
              <Icon name="headset" 
                  // type='ionicons'
                  // color='black'
              />
              TechTube
            </Text>
            <LogIn loginFuncProp = {this.logInFunc}/>
          </View>
          <Container>
            {this.renderVideo()}
          </Container>
        </Container>
          <Container style={styles.searchListStyle}>
           <SearchList  searchListprop = {this.renderSearchList()}/>
          </Container>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  headLoginView: {
    flexDirection: 'row',
    height: 30,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#e8f441'
  },
  searchBarHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 60,
    backgroundColor: '#e8f441'
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  buttonStyle: {
    backgroundColor: 'black',
    borderRadius: 20,
    width: 110,
    height: 35,
    marginLeft: 10
  }
});
