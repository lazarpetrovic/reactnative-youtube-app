import { Container, Header, Left, Body, Right, Button, Icon, Item, Input, Text, Title, Content, List, ListItem, Thumbnail } from 'native-base';
import React, { Component } from 'react';
import { Platform, StyleSheet, View } from 'react-native';

const LogIn = (props) => {
  return (
       <Icon
        name='login'
        type='Entypo'
        color='black'
        onPress={ props.loginFuncProp } />
  )
}

export default LogIn;