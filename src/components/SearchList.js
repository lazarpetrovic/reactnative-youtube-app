import { Container, Header, Left, Body, Right, Button, Icon, Item, Input, Text, Title, Content, List, ListItem, Thumbnail } from 'native-base';
import React, { Component } from 'react';
import { Platform, StyleSheet, View } from 'react-native';


const SearchList = (props) => {
  return (
      <Content style={styles.searchListStyle}>
        <List >
          {props.searchListprop}
        </List>
      </Content>
  )
}

export default SearchList;


const styles = StyleSheet.create({
  searchListStyle: {
    backgroundColor: 'grey',
  },
})